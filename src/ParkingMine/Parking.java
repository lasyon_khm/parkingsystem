package ParkingMine;

import java.util.ArrayList;

public class Parking {
    int randomCars = 1 + (int) (Math.random() * getCarParkingPlacesCount() / 3);
    int randomTrucks = 1 + (int) (Math.random()*getTruckParkingPlacesCount() / 3);

    ArrayList<CarPlace> carPlaceArrayList = new ArrayList<>(randomCars);

    private int carParkingPlacesCount;
    private int freeParkingPlaces;

    public Parking(int carParkingPlacesCount) {
        this.carParkingPlacesCount = carParkingPlacesCount;
        this.carPlaceArrayList = new ArrayList<CarPlace>();
        clearParking();
    }

    public int getCarParkingPlacesCount() {
        return carParkingPlacesCount;
    }

    public void placeFree(Car car) {
        for (CarPlace parkingCarPlace : carPlaceArrayList) {
            if (parkingCarPlace != null && parkingCarPlace.getCar() != null && parkingCarPlace.getCar().getId() == car.getId()) {
                carPlaceArrayList.remove(parkingCarPlace);
                freeParkingPlaces++;
                break;
            }
        }
    }

    public void setCarOnParkingPlace(Car car) {
        for (CarPlace parkingCarPlace : carPlaceArrayList) {
            if (parkingCarPlace == null) {
                freeParkingPlaces--;
                parkingCarPlace.setCar(car);
                break;
            }
        }
    }

    public int getCountFreeParkingPlaces() {
        return freeParkingPlaces;
    }

    public int placeTakedByCar() {
        return freeParkingPlaces--;
    }

    public void getInfoAboutCars() {
        System.out.println("Places left in car parking: " + getCountFreeParkingPlaces() +
                ", occupied places:" + getCountOccupiedPlaces());
    }

    public int getCountOccupiedPlaces() {
        return carParkingPlacesCount - freeParkingPlaces;
    }

    public void clearParking() {
        carPlaceArrayList.clear();
        freeParkingPlaces = carParkingPlacesCount;
    }




    ArrayList<CarPlace> truckPlaceArrayList = new ArrayList<CarPlace>(randomTrucks);

    private int truckParkingPlacesCount;
    private int truckFreeParkingPlaces;

    public void TruckParking(int truckParkingPlacesCount) {
        this.truckParkingPlacesCount = truckParkingPlacesCount;
        this.truckPlaceArrayList = new ArrayList<CarPlace>();
        clearTruckParking();
    }

    public void truckPlaceFree(Truck truck) {
        for (CarPlace parkingTruckPlace : truckPlaceArrayList) {
            if (parkingTruckPlace != null && parkingTruckPlace.getTruck() != null && parkingTruckPlace.getTruck().getTruckNumber() == truck.getTruckNumber()) {
                truckPlaceArrayList.remove(parkingTruckPlace);
                truckFreeParkingPlaces++;
                break;
            }
        }
    }

    public void setTruckOnParkingPlace(Truck truck) {
        if (getTruckFreeParkingPlaces() != 0) {
            for (CarPlace parkingTruckPlace : truckPlaceArrayList) {
                parkingTruckPlace.setTruck(truck);
                break;
            }
        } else {
            if (getCountFreeParkingPlaces() == 0 && getCountFreeParkingPlaces() >= 2) {
                for (CarPlace parkingCarPlace : carPlaceArrayList) {
                    freeParkingPlaces -= 2;
                    parkingCarPlace.setTruck(truck);
                    break;
                }
            }
        }
    }

    public int placeTakedByTruck() {
        return truckFreeParkingPlaces--;
    }

    public void getInfoAboutTrucks() {
        System.out.println("Places left in car parking: " + getTruckFreeParkingPlaces() +
                ", occupied places:" + getCountTruckOccupiedPlaces());
    }

    public void clearTruckParking() {
        truckPlaceArrayList.clear();
        truckFreeParkingPlaces = truckParkingPlacesCount;
    }

    public int getCountTruckOccupiedPlaces() {
        return truckParkingPlacesCount - truckFreeParkingPlaces;
    }

    public int getTruckParkingPlacesCount() {
        return truckParkingPlacesCount;
    }

    public void setTruckParkingPlacesCount(int truckParkingPlacesCount) {
        this.truckParkingPlacesCount = truckParkingPlacesCount;
    }

    public int getTruckFreeParkingPlaces() {
        return truckFreeParkingPlaces;
    }

    public void setTruckFreeParkingPlaces(int truckFreeParkingPlaces) {
        this.truckFreeParkingPlaces = truckFreeParkingPlaces;
    }
}
