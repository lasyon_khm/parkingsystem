package ParkingMine;

public class Truck {
    private int truckTurn;
    private int truckNumber;

    public Truck(int turn, int id) {
        this.truckTurn = truckTurn;
        this.truckNumber = truckNumber;
    }


    public int getTruckTurn() {
        return truckTurn;
    }

    public void setTruckTurn(int truckTurn) {
        this.truckTurn--;
    }

    public int getTruckNumber() {
        return truckNumber;
    }

    public void setTruckNumber(int truckNumber) {
        this.truckNumber = truckNumber;
    }

}