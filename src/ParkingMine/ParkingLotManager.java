package ParkingMine;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class ParkingLotManager {

    public static void main(String[] args) {
        int id = 1000;
        int truckNumber = 2000;
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);


        System.out.println("Enter the number of parking spaces for cars");
        int parkingPlacesForCars = scanner.nextInt() - 1;
        System.out.println("Enter the number of parking spaces for trucks");
        int parkingPlacesForTrucks = scanner.nextInt() - 1;
        Parking parking = new Parking(parkingPlacesForCars);
        parking.TruckParking(parkingPlacesForTrucks);
        System.out.println("You can choose \n1 for make a turn \n2 to get info about parking " +
                "\n3 to clear parking from cars");
        while (true) {
            if (parking.getCountFreeParkingPlaces() == 0) {
                System.out.println("Parking doesn't have any spaces");
            }
            System.out.println("Enter the command");
            int command = scanner.nextInt();
            switch (command) {
                case 1:
                    for (Car car : createCars(random.nextInt(10), id)) {
                        System.out.println("Car can enter the parking lot ");
                        System.out.println("Car is in the parking");
                        parking.setCarOnParkingPlace(car);
                        System.out.println("Places left: " + parking.placeTakedByCar());
                    }
                    System.out.println("\n");
                    for (Truck truck : createTrucks(random.nextInt(10), truckNumber)) {
                        System.out.println("Truck can enter the parking lot");
                        System.out.println("Truck is in the parking");
                        parking.setTruckOnParkingPlace(truck);
                        System.out.println("Places left: " + parking.placeTakedByTruck());
                    }
                    System.out.println("\n");
                    System.out.println("Now we have cars. There are: " + parking.getCountOccupiedPlaces());
                    for (Car car : createCars(random.nextInt(10), id)) {
                        car.doTurn();
                        if (car.getTurn() == 0) {
                            parking.placeFree(car);
                        }
                    }
                    System.out.println("Now we have trucks. There are:" + parking.getCountTruckOccupiedPlaces());
                    for (Truck truck : createTrucks(random.nextInt(10), truckNumber)) {
                        truck.setTruckTurn(truck.getTruckTurn());
                        if (truck.getTruckTurn() == 0) {
                            parking.truckPlaceFree(truck);
                        }
                    }
                    break;
                case 2:
                    System.out.println("Print info");
                    parking.getInfoAboutCars();
                    parking.getInfoAboutTrucks();
                    break;
                case 3:
                    parking.clearParking();
                    parking.clearTruckParking();
                    System.out.println("Parking cleared");
                    break;
                default:
                    System.out.println("Unidentified command");
            }
        }
    }

    static Random random = new Random();
    static Parking parking1 = new Parking(random.nextInt(100));

    private static int randomCar = 1 + (int) (Math.random() * parking1.getCarParkingPlacesCount() / 3);
    private static int randomTruck = 1 + (int) (Math.random() * parking1.getTruckParkingPlacesCount() / 3);

    public static ArrayList<Car> createCars(int turn, int id) {
        Random random = new Random();
        ArrayList<Car> cars = new ArrayList<>(randomCar);
        for (int i = 0; i < randomCar; i++) {
            cars.add(new Car(random.nextInt(10), id));
        }
        return cars;
    }

    public static ArrayList<Truck> createTrucks(int truckTurn, int truckNumber) {
        Random random = new Random();
        ArrayList<Truck> trucks = new ArrayList<>(randomTruck);
        for (int i = 0; i < randomTruck; i++) {
            trucks.add(new Truck(random.nextInt(10), truckNumber));
        }
        return trucks;

    }
}
