package ParkingMine;

public class CarPlace {
    private Car currentCar;
    private Truck currentTruck;

    public CarPlace() {
        currentCar = null;
        currentTruck = null;
    }

    public boolean setCar(Car currentCar) {
        if (this.currentCar == null) {
            this.currentCar = currentCar;
        }
        return this.currentCar.equals(currentCar);
    }

    public boolean setTruck(Truck currentTruck) {
        if (this.currentTruck == null) {
            this.currentTruck = currentTruck;
        }
        return this.currentTruck.equals(currentTruck);
    }

    public Truck getTruck() {
        return currentTruck;
    }

    public Car getCar() {
        return currentCar;
    }
}